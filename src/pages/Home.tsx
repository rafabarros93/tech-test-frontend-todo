import {Container, List} from '@mui/material'
import React, {useState} from 'react'
import Form from '../components/Form'
import TodoItem from '../components/TodoItem'
import logo from '../assets/logo.svg'
import './home.css'
import {addTodo, deleteTodo, editTodo} from '../handlers/todos'

export default function Home() {
  const [todos, setTodos] = useState([])

  const addTodoList = todo => {
    let newTodo: {}[] = todos

    let todoList: {}[] = addTodo(todo, newTodo)

    setTodos(todoList)

    return todos
  }

  const deleteTodoList = (id: number): void => {
    let filtered = deleteTodo(id, todos)
    setTodos(filtered)
  }

  const editTodoList = (id: number, editedText: string, statusTask: boolean): void => {
    let todosArray: [] = editTodo(todos, id, editedText, statusTask)

    setTodos(todosArray)
  }

  return (
    <div className="home">
      <header className="home-header">
        <img src={logo} className="home-logo" alt="logo" />
        <Container maxWidth="xs" style={{marginTop: '1em'}}>
          <Form addTodo={addTodoList} />
          <List sx={{marginTop: '1em'}}>
            {todos.map(todo => (
              <div key={todo.id} style={{marginTop: '1em'}}>
                <TodoItem editTodo={editTodoList} todo={todo} deleteTodo={deleteTodoList} />
              </div>
            ))}
          </List>
        </Container>
      </header>
    </div>
  )
}
