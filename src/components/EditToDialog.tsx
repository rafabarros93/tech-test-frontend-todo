import * as React from 'react'
import Button from '@mui/material/Button'
import Dialog from '@mui/material/Dialog'
import DialogActions from '@mui/material/DialogActions'
import DialogContent from '@mui/material/DialogContent'
import DialogTitle from '@mui/material/DialogTitle'
import Slide from '@mui/material/Slide'
import {TextField} from '@mui/material'
import MenuItem from '@mui/material/MenuItem'
import {status} from '../helpers/status'

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />
})

export default function EditTodoDialog({open, dialogHandler, todo, editTodo}) {
  const [editedText, setEditedText] = React.useState(todo.text)
  const [editedStatusTask, setEditedstatusTask] = React.useState(todo.statusTask)
  const textHandler = (): void => {
    editTodo(todo.id, editedText, editedStatusTask)
    dialogHandler()
  }

  return (
    <Dialog open={open} TransitionComponent={Transition} keepMounted onClose={dialogHandler} aria-describedby="alert-dialog-slide-description" fullWidth>
      <DialogTitle>{editedText}</DialogTitle>
      <DialogContent>
        <div>
          <TextField defaultValue={editedText} fullWidth onChange={e => setEditedText(e.target.value)} />
        </div>
        <div style={{padding: '1em 3px 30px 5px'}}>
          <TextField id="outlined-select-currency" select label="Status" value={editedStatusTask} onChange={(e): void => setEditedstatusTask(e.target.value)}>
            {status.map(option => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </TextField>
        </div>
      </DialogContent>
      <DialogActions>
        <Button onClick={dialogHandler}>Cancelar</Button>
        <Button onClick={textHandler}>Ok</Button>
      </DialogActions>
    </Dialog>
  )
}
