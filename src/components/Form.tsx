import React, {useState} from 'react'
import {Button, Paper, TextField} from '@mui/material'
import {format} from 'date-fns'
import MenuItem from '@mui/material/MenuItem'
import {status} from '../helpers/status'

export default function Form({addTodo}) {
  const [text, setText] = useState(null)

  const [id, setId] = useState(0)
  const [statusTask, setTask] = React.useState(false)

  const todoCreate = (text: any, statusTask: any): void => {
    let date: string = format(new Date(), 'dd/MM/yyyy')

    const todoObj = {text, date, statusTask, id: id}

    console.log(todoObj)
    setId(id + 1)
    addTodo(todoObj)
    let doc: HTMLHtmlElement = document.getElementById('outlined-basic')

    doc.value = null
  }

  return (
    <Paper style={{padding: '1em'}}>
      <div style={{display: 'flex', justifyContent: 'center'}}>
        <TextField id="outlined-basic" label="Tarefa" required variant="outlined" onChange={(e): void => setText(e.target.value)} fullWidth />

        <div>
          <TextField id="outlined-select-currency" select label="Status" value={statusTask} onChange={(e): void => setTask(e.target.value)}>
            {status.map(option => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </TextField>
        </div>

        <Button variant="text" onClick={(): void => todoCreate(text, statusTask)}>
          Add
        </Button>
      </div>
    </Paper>
  )
}
