export const status = [
  {
    value: true,
    label: 'concluída',
  },
  {
    value: false,
    label: 'pendente',
  },
]
