export const addTodo = (todo: {}, todos: {}[]): {}[] => {
  let addTodo: {}[] = [...todos, todo]

  return addTodo
}

export const deleteTodo = (id: number, todos: any): any => {
  let filtered = todos.filter((todo: {text: string; date: string; statusTask: boolean; id: number}): boolean => todo.id !== id)

  return filtered
}

export const editTodo = (todos: any, id: number, editedText: string, statusTask: boolean): [] => {
  todos.forEach((value: any, index: number): void => {
    if (value.id == id) {
      todos[index].text = editedText
      todos[index].statusTask = statusTask
    }
  })

  return todos
}
