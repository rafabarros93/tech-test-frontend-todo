import {addTodo, deleteTodo, editTodo} from './todos'
import {format} from 'date-fns'

describe('Create todo', (): void => {
  it('Should add a new todo', (): void => {
    let todos: [] = []

    const todo: {text: string; date: string; statusTask: boolean; id: number} = {text: 'New Task', date: format(new Date(), 'dd/MM/yyyy'), statusTask: true, id: 1}

    let addTodoLIst: any = addTodo(todo, todos)

    let arrayExpect: [{text: string; date: string; statusTask: boolean; id: number}] = [{text: 'New Task', date: format(new Date(), 'dd/MM/yyyy'), statusTask: true, id: 1}]

    expect(arrayExpect).toEqual(expect.arrayContaining(addTodoLIst))
  })

  it('Should remove by id todo', (): void => {
    let todos: [{text: string; date: string; statusTask: boolean; id: number}, {text: string; date: string; statusTask: boolean; id: number}] = [
      {text: 'New Task', date: format(new Date(), 'dd/MM/yyyy'), statusTask: true, id: 1},
      {text: 'New Task', date: format(new Date(), 'dd/MM/yyyy'), statusTask: true, id: 2},
    ]

    let deleteTodoLIst: any = deleteTodo(1, todos)

    let expectedArray: [{text: string; date: string; statusTask: boolean; id: number}] = [{text: 'New Task', date: format(new Date(), 'dd/MM/yyyy'), statusTask: true, id: 2}]

    expect(expectedArray).toEqual(expect.arrayContaining(deleteTodoLIst))
  })

  it('Should edit todo', (): void => {
    let todos: [{text: string; date: string; statusTask: boolean; id: number}, {text: string; date: string; statusTask: boolean; id: number}] = [
      {text: 'New Task', date: format(new Date(), 'dd/MM/yyyy'), statusTask: true, id: 1},
      {text: 'New Task', date: format(new Date(), 'dd/MM/yyyy'), statusTask: false, id: 2},
    ]

    let editedText: string = 'New Task Edited'
    let statusTask: boolean = false

    let editTolist: any = editTodo(todos, 2, editedText, statusTask)

    let editedTodo = editTolist.find((todo: {text: string; date: string; statusTask: boolean; id: number}) => todo.id == 2)

    expect(editedText).toEqual(editedTodo.text)
    expect(statusTask).toEqual(editedTodo.statusTask)
  })
})
